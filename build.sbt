name := "frbSBTPlugin"

organization := "de.opal-project"

homepage := Some(url("http://www.opal-project.de"))

licenses := Seq("BSD-2-Clause" -> url("http://opensource.org/licenses/BSD-2-Clause"))

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.4"

scalaVersion in frbPlugin := "2.11.1"

sbtPlugin := true

//extractJarsTarget in frbPlugin <<= (resourceDirectory in Compile)

extractJarsTarget in frbPlugin := baseDirectory.value / "automatic-resources"

unmanagedResourceDirectories in Compile += baseDirectory.value / "automatic-resources"


publishLocal <<= publishLocal.dependsOn(extractJars in frbPlugin)

//
//
// SETTINGS REQUIRED TO PUBLISH OPAL ON MAVEN CENTRAL
//
//

publishMavenStyle in ThisBuild := true

publishTo in ThisBuild := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}

publishArtifact in Test := false

pomExtra in ThisBuild := (
  <scm>
    <url>git@bitbucket.org:delors/findrealbugs-sbt-plugin.git</url>
    <connection>scm:git:git@bitbucket.org:delors/findrealbugs-sbt-plugin.git</connection>
  </scm>
  <developers>
    <developer>
      <id>Moghimi</id>
      <name>Babak Moghimi</name>
      <url>http://moghimi.de</url>
    </developer>
    <developer>
        <id>eichberg</id>
        <name>Michael Eichberg</name>
        <url>http://www.michael-eichberg.de</url>
    </developer>
  </developers>)
