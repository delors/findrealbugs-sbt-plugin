/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/**
 *  This is a custom class loader for the plugin in which we search not only our direct parent but also ourselves for the findrealbugs class
 *  which is in scala 2.11
 * 	@author Babak Moghimi
 * *
 */
package org.opalj
package frb
package sbtplugin

import java.net.URL
import java.net.URLClassLoader
import java.lang.ClassLoader
import java.util.{ List ⇒ JavaList }
import scala.collection.JavaConverters._

class PluginClassLoader(classpath: List[URL], childClassLoader: ChildClassLoader) extends ClassLoader(Thread.currentThread().getContextClassLoader()) {

    /* The user just passes the list of classpath's to be search; we then map it to a useful constructor 
     * @param A Scala List of URL's which will then be tranformed to java array
     */
    def this(classpath: List[URL]) =
        this(classpath,
            new ChildClassLoader(
                classpath.asJava.toArray(Array[URL]()),
                new DetectClass(getClass.getSuperclass.getClassLoader.asInstanceOf[ClassLoader])
            )
        )

    override final def loadClass(name: String, resolve: Boolean): Class[_] = {
        try {
            this.childClassLoader.findClass(name)
        } catch {
            case e: ClassNotFoundException ⇒ super.loadClass(name, resolve).asInstanceOf[Class[_]]
        }
    }

}

private class ChildClassLoader(urls: Array[URL], realParent: DetectClass) extends URLClassLoader(urls, null) {

    /*
	 * We search the parent and the child for the class because we have multiple classpath's set
	 */
    @Override
    override def findClass(name: String): Class[_] =
        try {
            val loaded: Class[_] = super.findLoadedClass(name).asInstanceOf[Class[_]]
            if (loaded ne null)
                loaded
            else
                super.findClass(name).asInstanceOf[Class[_]]
        } catch {
            case e: ClassNotFoundException ⇒ realParent.loadClass(name).asInstanceOf[Class[_]]
        }

}

private class DetectClass(val parent: ClassLoader) extends java.lang.ClassLoader(parent) {

    @Override
    override def findClass(name: String): Class[_] = super.findClass(name).asInstanceOf[Class[_]]

}

