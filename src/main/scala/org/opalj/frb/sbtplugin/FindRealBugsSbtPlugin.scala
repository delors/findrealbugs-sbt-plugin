/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/**
 *  This is a plugin for SBT which allows you to configure and run FindRealBugs Analysis on your scala project.
 * 	@author Babak Moghimi
 * *
 */
package org.opalj
package frb
package sbtplugin

import sbt._
import Classpaths.managedJars
import Keys._

import java.net.URLClassLoader
//import java.util._
import java.io.File
import scala.collection.JavaConversions
import scala.reflect._

object FindRealBugsSbtPlugin extends Plugin {

    /**
     * Gets the analysis and the classfiles which should be analysed and runs the analysis sbt plugin task
     * @param an Array of all the analysis that should NOT be run (To be disabled)
     * @param the location folder of the classfiles
     */
    def running(analysisList: Array[String], classFiles: String, librarySeq: Seq[String]) {

        //At the time of development sbt was compiled against scala 2.10 and opal was developed using scala 2.11 so this approach was called upon
        //First we obtain a reference to the compiled FRB resource that is placed in src/main/resources 
        val thisCLass = getClass.getResource("FindRealBugsSbtPlugin.class").toString()
        val thisJar = thisCLass.substring(0, thisCLass.lastIndexOf('!'))+"!/"
        //it is better if we send a scala List to the classLoader and process it there
        val path: scala.collection.immutable.List[java.net.URL] = List(new URL(thisJar))

        //then we use a custom class loader to load it beside the default classLoader
        //val cl = new CustomClassLoader(path)
        val cl = new PluginClassLoader(path)
        val FindRealBugs = cl.loadClass("org.opalj.frb.cli.FindRealBugsCLI$")
        val mainMethod = FindRealBugs.getDeclaredMethod("main", Array(classOf[Array[String]]): _*)
        val instance = FindRealBugs.getDeclaredField("MODULE$")
        val fetchedInstance = instance.get(null)
        //we need to add -i to every analysis that is going to be ignored and return a new list
        //if (!analysis.isEmpty)
        val analysis = generateAnalysis(analysisList)
        var libraries = Array[String]()
        if(!librarySeq.isEmpty)
            libraries = generateLibraries(librarySeq)
        val libraryParams = libraries ++ analysis
        val params: Array[String] = libraryParams :+ classFiles
        //Finally we call CLI's main method to run the analysis for us
        mainMethod.invoke(fetchedInstance, params)

    }
    
    def generateLibraries(args:Seq[String]): Array[String] = {
        
            var list = Array[String]()
            for (item <- args) {
                 
                list = list :+ "-l="+item
            }
        
             return list
        
    }
    def generateAnalysis(args: Array[String]): Array[String] = {
        /*
             * A list of available analysis
             */
        val builtInAnalyses: Array[String] = Array(
            "AnonymousInnerClassShouldBeStatic",
            "BadlyOverriddenAdapter",
            "BitNops",
            "BoxingImmediatelyUnboxedToPerformCoercion",
            "CatchesIllegalMonitorStateException",
            "CloneDoesNotCallSuperClone",
            "CnImplementsCloneButNotCloneable",
            "CovariantCompareTo",
            "CovariantEquals",
            "DmRunFinalizersOnExit",
            "DoInsideDoPrivileged",
            "EqualsHashCodeContract",
            "FieldIsntImmutableInImmutableClass",
            "FieldShouldBeFinal",
            "FieldShouldBePackageProtected",
            "FinalizeUseless",
            "ImmutableClassInheritsMutableClass",
            "ImplementsCloneableButNotClone",
            "InefficientToArray",
            "LongBitsToDoubleInvokedOnInt",
            "NativeMethodInImmutableClass",
            "NonSerializableClassHasASerializableInnerClass",
            "ManualGarbageCollection",
            "ProtectedFieldInFinalClass",
            "PublicFinalizeMethodShouldBeProtected",
            "SerializableNoSuitableConstructor",
            "SwingMethodInvokedInSwingThread",
            "SyncSetUnsyncGet",
            "UninitializedFieldAccessDuringStaticInitialization",
            "UnusedPrivateFields",
            "UrUninitReadCalledFromSuperConstructor",
            "UselessIncrementInReturn"
        )
        if (args.isEmpty) return args
        else {
            var list = Array[String]()
            for (item ← args)
                    list = list :+ "--analysis=org.opalj.frb.analyses."+item
            return list
        }
    }

    
    //These are settings and tasks that the plugin will export
    val runFrbAnalysis = taskKey[Unit]("running frb analysis for you")
    val analysisList = settingKey[Array[String]]("a list of all possible analysis")
    val libraryList = taskKey[Seq[String]]("a list of files to be added as libraries")
    val classFiles = settingKey[String]("a list of classes to run the analysis against")
    
    
    val frbSettings = Seq(
        classFiles := (classDirectory in Compile).value.toString,
        analysisList := Array[String](),
        libraryList <<= 
        { (classpathTypes, update) map { (ct, up) ⇒
            managedJars(Compile, ct, up) map { _.data.getPath } 
        }},
        runFrbAnalysis := running(analysisList.value, classFiles.value, libraryList.value)
    )

}